﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class ImplKosmita:IKobieta,IKosmita
    {
        public ImplKosmita() { }

        public void Malowanie()
        {
            Console.WriteLine("kosmitka kobieta sie maluje");
        }

        public string Idz()
        {
            return "Kosmitka kobieta idzie";
        }

        string IKosmita.Idz()
        {
            return "Idz z IKosmita";
        }

        string ICzłowiek.Idz()
        {
            return "Wywalowane Idz z Czlowieka";
        }

        

        public void Odlatuj()
        {
            Console.WriteLine("Kosmitka kobieta odlatuje");
        }
    }
}
