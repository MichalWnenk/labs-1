﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class ImplMężczyzna : IMężczyzna
    {
        public ImplMężczyzna()
        { }

        public void Pakowanie()
        {
            Console.WriteLine("Mezczyzna sie pakuje");
        }

        public string Idz()
        {
            return "Mezczyzna idzie";
        }
    }
}
