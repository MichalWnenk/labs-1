﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Implementation;
using Lab1.Contract;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ICzłowiek);
        
        public static Type ISub1 = typeof(IKobieta);
        public static Type Impl1 = typeof(ImplKobieta);
       
        public static Type ISub2 = typeof(IMężczyzna);
        public static Type Impl2 = typeof(ImplMężczyzna);
        
        
        public static string baseMethod = "Idz";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Malowanie";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Pakowanie";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "tworzenieKolekcji";
        public static string collectionConsumerMethod = "odpytajKolekcje";

        #endregion

        #region P3

        public static Type IOther = typeof(IKosmita);
        public static Type Impl3 = typeof(ImplKosmita);

        public static string otherCommonMethod = "Idz";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
