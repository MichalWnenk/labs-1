﻿using System;
using System.Collections.Generic;
using System.Collections;
using Lab1.Contract;
using Lab1.Implementation;


namespace Lab1.Main
{
    public class Program
    {
      
        static void Main(string[] args)
        {
            

        }

        public IList<ICzłowiek> tworzenieKolekcji()
        {
            List<ICzłowiek> lista = new List<ICzłowiek>();
            lista.Add(new ImplKobieta());
            lista.Add(new ImplMężczyzna());
            lista.Add(new ImplKobieta());
            lista.Add(new ImplMężczyzna());
            lista.Add(new ImplKobieta());
            lista.Add(new ImplMężczyzna());
            return lista;
        }

        public void odpytajKolekcje(IList<ICzłowiek> lista)
        {
            foreach (var item in lista)
            {
                item.Idz(); 
            }
        }
    }
}
