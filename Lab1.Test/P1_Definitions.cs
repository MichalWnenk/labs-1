﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

using Lab1.Main;
using Lab1.Implementation;
using Lab1.Implementation;
using PK.Test;

namespace Lab1.Test
{
    [TestFixture]
    [TestClass]
    public class P1_Definitions
    {
        #region IBase

        [Test]
        [TestMethod]
        public void P1__IBase_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.IBase.IsInterface);
        }
        
        [Test]
        [TestMethod]
        public void P1__IBase_Should_Have_Methods()
        {
            Helpers.Should_Have_Methods(LabDescriptor.IBase);
        }

        [Test]
        [TestMethod]
        public void P1__IBase_Should_Have_BaseMethod()
        {
            Helpers.Should_Have_Method(LabDescriptor.IBase, LabDescriptor.baseMethod);
        }

        #endregion

        #region ISub1

        [Test]
        [TestMethod]
        public void P1__ISub1_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.ISub1.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__ISub1_Should_Inherit_From_IBase()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.ISub1, LabDescriptor.IBase);
        }

        [Test]
        [TestMethod]
        public void P1__ISub1_Should_Have_Methods()
        {
            Helpers.Should_Have_Methods(LabDescriptor.ISub1);
        }

        [Test]
        [TestMethod]
        public void P1__ISub1_Should_Have_Sub1Method()
        {
            Helpers.Should_Have_Method(LabDescriptor.ISub1, LabDescriptor.sub1Method);
        }

        #endregion

        #region Impl1

        [Test]
        [TestMethod]
        public void P1__Impl1_Should_Be_A_Class()
        {
            // Assert
            Assert.That(LabDescriptor.Impl1.IsClass);
        }

        [Test]
        [TestMethod]
        public void P1__Impl1_Should_Inherit_From_ISub1()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.Impl1, LabDescriptor.ISub1);
        }

        [Test]
        [TestMethod]
        public void P1__Impl1_Should_Implement_BaseMethod()
        {
            Helpers.Should_Implement_Method(LabDescriptor.Impl1, LabDescriptor.baseMethod, LabDescriptor.baseMethodParams);
        }

        [Test]
        [TestMethod]
        public void P1__Impl1_Should_Implement_Sub1Method()
        {
            Helpers.Should_Implement_Method(LabDescriptor.Impl1, LabDescriptor.sub1Method, LabDescriptor.sub1MethodParams);
        }

        #endregion

        #region ISub2

        [Test]
        [TestMethod]
        public void P1__ISub2_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.ISub2.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__ISub2_Should_Inherit_From_IBase()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.ISub2, LabDescriptor.IBase);
        }

        [Test]
        [TestMethod]
        public void P1__ISub2_Should_Have_Methods()
        {
            Helpers.Should_Have_Methods(LabDescriptor.ISub2);
        }

        [Test]
        [TestMethod]
        public void P1__ISub2_Should_Have_Sub2Method()
        {
            Helpers.Should_Have_Method(LabDescriptor.ISub2, LabDescriptor.sub2Method);
        }

        #endregion

        #region Impl2

        [Test]
        [TestMethod]
        public void P1__Impl2_Should_Be_A_Class()
        {
            // Assert
            Assert.That(LabDescriptor.Impl2.IsClass);
        }

        [Test]
        [TestMethod]
        public void P1__Impl2_Should_Inherit_From_ISub2()
        {
            Helpers.Should_Implement_Interface(LabDescriptor.Impl2, LabDescriptor.ISub2);
        }

        [Test]
        [TestMethod]
        public void P1__Impl2_Should_Implement_BaseMethod()
        {
            Helpers.Should_Implement_Method(LabDescriptor.Impl2, LabDescriptor.baseMethod, LabDescriptor.baseMethodParams);
        }

        [Test]
        [TestMethod]
        public void P1__Impl2_Should_Implement_Sub2Method()
        {
            Helpers.Should_Implement_Method(LabDescriptor.Impl2, LabDescriptor.sub2Method, LabDescriptor.sub2MethodParams);
        }

        #endregion
    }
}




